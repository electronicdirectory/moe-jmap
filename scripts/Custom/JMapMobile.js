var mapData; var myAmenities = []; var baseUrl; var zoomlevelnumbers = 30;
//search level 
//1 for searching
//2 for from_searching
//3 for to_searching
var searchLevel = 1; var selectionZoomLevel = 0; var highlightColor = '#e40404'; var isPathDisplay = false; var swingToggleInterval = 700;
var building;
//configuration for Catgories
var categories = []; var destinations = []; var selectedDestName = ''; var selectedDestId = 0; var htmlStr = ''; var isElevator = false; var commonTimeOut = 3000; var CategoryId = 0;
//Zoom In and Out Configuration 			
var interval = 0.2; var speed = 0.1; var engine; var removeButtonAnimation = 500; var marchAntInterval; var setCountReadPathUI = 0; var maxZoomLimit = 6; var anchorStoreId = 0;

var configuration = {
    mapStyles: {
        mapLayers: [
            {
                "name": "Units-Layer",
                "fill": "#faeed7",
                "stroke": "#BABABA",
                "strokeWidth": "2",
                "clickable": "true",

            },
            {
                "name": "Obstacles-Layer",
                "fill": "#F4F4F4",
                "stroke": "#758d92",
                "strokeWidth": "0.5",
                "opacity": "1"
            },
            {
                "name": "Streets-Major-Layer",
                "fill": "none",
                "stroke": "#5F4F4C",
                "strokeWidth": "86",
                "strokeLinejoin": "round",
                "strokeMeterlimit": "10"
            },
            {
                "name": "Streets-Minor-Layer",
                "fill": "none",
                "stroke": "#5F4F4C",
                "strokeWidth": "86",
                "strokeLinejoin": "round",
                "strokeMeterlimit": "10"
            },
            {
                "name": "Streets-SmallAlleys-Layer",
                "fill": "none",
                "stroke": "#5F4F4C",
                "strokeWidth": "10",
                "strokeLinejoin": "round",
                "strokeMeterlimit": "10"

            },
            {
                "name": "Elevators-Layer",
                "fill": "#5F4F4C",
                "stroke": "#5F4F4C"
            },
            {
                "name": "Mall-Boundary-Layer",
                "fill": "#F8EDE1",
                "stroke": "#BABABA",
                "strokeWidth": "5",
                "opacity": "0.8"
            },
            {
                "name": "Background-Layer",
                "fill": "#F7F7F7"
            },
            {
                "name": "Corridor-Indoor-Layer",
                "fill": "#5f4f4c",
                "stroke": "#5f4f4c",
                "strokeWidth": "10",
            },
      {
          "name": "Parking-Lots-Layer",
          "fill": "#354F3A"
      },
      {
          "name": "Pattern-OutdoorTerrace-Layer",
          "fill": "none",
          "stroke": "#A0A0A0",
          "strokeWidth": "10",
          "strokeLinejoin": "round",
          "strokeMiterlimit": "0"
      }
        ],

        mapConfig: {
            engineOptions: {
                // Here you can set the min/max scale defaults.
                //maxScale: 4.6,
                //minScale: 0.3
            },
            mapLabels: {
                showLogos: false
            }
        },
        iconStyles: {
            youarehere: {
                url: "images/start.png",
                width: 25,
                height: 25,
                allowZoomScale: "true",
                offset: {
                    x: -12.5,
                    y: -12.5
                }
            },
            destination: {
                url: "images/dest.png",
                width: 25,
                height: 25,
                offset: {
                    x: -12.5,
                    y: -12.5
                }
            },
            "legends": {
                "importSVGMarkup": true
            }
        },
        "svg": {
            "dropShadow": {
                "useShadow": false,
                "shadowStyle": {
                    "opacity": 0.1,
                    "dx": 0,
                    "dy": 2
                }
            }
        }
    }
}
$(document).ready(function () {
    //ConsumeMOEService();
    //soap();
    baseUrl = $('#hdnBaseURL').val() + '/';
    // Initialize JMap
    JMap.init({
        url: $('#hdnBaseURL').val(),
        id: $('#hdnId').val(),
        locationId: $('#hdnLocationId').val(),
        width: '100%',
        height: '100%',
        apiUser: $('#hdnapiUser').val(),
        apiPass: $('#hdnapiPass').val(),
        apiKey: $('#hdnapiKey').val(),
        code: $('#hdnCode').val(),
        pingYah: false,
        config: configuration,
        container: "#canvasContainer",
        allMapsRendered: onMapsRendered
    });
    
    function onMapsRendered(b) {
        //setPolygonFit();
        clearAllMobileSubMenu();
        building = b;
        $("#loadingScreen").hide();
        //Bind the categories
        JMap.getCategories(function (res) {
            categories = res;
            //populateCategories(categories);
            BindCategories(categories);
        });

        JMap.getDestinations(function (res) {
            populate(res);
            destinations = res;
            populateStore(res)

        });
        engine = b.engineDelegate

        BindFloors(building.mapsData);
        Amentities();
        
        building.yahImage = JMap.configuration.mapStyles.iconStyles.youarehere;
        building.engineDelegate.options.doubletap = false;
        JMap.configuration.mapStyles.pathStyles.pathWidth = 7;
        JMap.configuration.mapStyles.pathStyles.pathOpacity = 1;
        setpolygonFitSize();
        building.addMapInteraction("touchend click", function (destination, waypoint, event) {
            addMapInteractionClick(destination[0], waypoint, event);
            //document.getElementsByClassName("destinationResult").onclick = interactiveParking
        });
        displaySourceFromParam();
    }
    $('#liAmenities').click(function () {
        clearAllMobileSubMenu();
        $('.mainMobileMenu > li').removeClass('resp-tab-active');
        $(this).addClass('resp-tab-active');
        //$('#amenities').show();
        $('#amenities').slideToggle(swingToggleInterval, "swing", false);
    });
    $('#liSearch').click(function () {
        clearAllMobileSubMenu();
        $('.mainMobileMenu > li').removeClass('resp-tab-active');
        $(this).addClass('resp-tab-active');
        //$('#dvSearchSubContainer').show();
        $('#dvSearchSubContainer').slideToggle(swingToggleInterval, "swing", false);
    });
    $('#liAttractions').click(function () {
        clearAllMobileSubMenu();
        $('.mainMobileMenu > li').removeClass('resp-tab-active');
        $(this).addClass('resp-tab-active');
        //$('#attractionContainer').show();
        $('#attractionContainer').slideToggle(swingToggleInterval, "swing", false);
    });
    $(document).on('keyup', '#moe_search_input_main', function (ev) {
        filterStoreResult($(this).val().toString().toLowerCase(), CategoryId);
    });
    $(document).on('click', '.categoryDropdown', function (ev) {
        CategoryId = $(this).attr('data-id');
        $('#displaySelectedCategory').text($(this).text());
        $(".category-drop").toggle();
        filterStoreResult($('#moe_search_input_main').val().toString().toLowerCase(), CategoryId);
    });
    $(document).on('click', '#replayButton', function (ev) {
        var _from = JMap.getDestinationById($("#Search-From").attr("data"))
        var _to = JMap.getDestinationById($("#Search-To").attr("data"))
        building.wayfind(_from, _to, {
            useElevator: isElevator, // Forces path with elevators only
            animate: true, // Animates the drawn path
            approxEntrance: false // Draws path to second last waypoint on path.
        });
        //setTimeout(function () {
        //    setRedPathLineUI();
        //}, commonTimeOut);
        //displayMarchingAnt();
    });
    $(document).on('click', '#accessiblePath', function (ev) {
        isElevator = !isElevator;
        //var imagePath = (isElevator ? '22337_1_5989_Accessible_Active.thumbnail.png' : '22335_1_5989_22337_1_5989_Accessibe_inActive.png');
        //$(this).attr("style", "background-image:url('Images/" + imagePath + "');");
        //var imagePath = (isElevator ? 'active' : '22335_1_5989_22337_1_5989_Accessibe_inActive.png');
        //$(this).attr("style", "background-image:url('Images/" + imagePath + "');");

        if (isElevator) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }

        var _from = JMap.getDestinationById($("#Search-From").attr("data"));
        var _to = JMap.getDestinationById($("#Search-To").attr("data"));
        displayWayAnimation(_from, _to);
        building.wayfind(_from, _to, {
            useElevator: isElevator, // Forces path with elevators only
            animate: true, // Animates the drawn path
            approxEntrance: false // Draws path to second last waypoint on path.
        });
        //setTimeout(function () {
        //    setRedPathLineUI();
        //}, commonTimeOut);
        //displayMarchingAnt();
        $(this).show();
    });
    $(document).on('click', '#amenityPopClose', function () {
        $('#amenityAlertBox').hide();
    });

    JMap.addListener('pathAnimationComplete', function () {
        setRedPathLineUI();
    });
    JMap.addListener('TextDirections', function (instructionsByFloor) {
        //var stepsElement = document.getElementById('instruction-container');

        var htmlMarkUP = '';


        //stepsElement.innerHTML = ''
        instructionsByFloor.forEach((instructions) => {
            // Add floor name title
            //var floorName = document.createElement('p')
            //floorName.className = 'moe_si_floor'
            //floorName.innerHTML = instructions[0].floorName

            //htmlMarkUP += '<div>';
            //var stepList = document.createElement('div')
            // Add instructions here
            instructions.forEach((inst) => {

                htmlMarkUP += "<div class='item'><div>" + inst.output + " - <b>" + instructions[0].floorName + "</b></div></div>";

                //var item = document.createElement('div')
                //item.innerHTML = "<div>" + inst.output + " - <b>" + instructions[0].floorName + "</b></div>"
                //item.className = 'item'
                //item.dataset.waypointId = inst.wp.id
                //item.addEventListener('click', focusWaypoint, false)
                ////stepList.appendChild(item)
                //stepsElement.appendChild(item)
            })
            //htmlMarkUP += '</div>';
            //stepsElement.appendChild(floorName)
            //stepsElement.appendChild(stepList)
        });

        $('#dvSliderInstruction').show();
        $('#instruction-container').show();


        //console.log(htmlMarkUP);
        var $owl = $('#instruction-container');
        $owl.trigger('destroy.owl.carousel');

        $.when($owl.html(htmlMarkUP).removeClass('owl-loaded')).then(function () {
            //$("#instruction-container").owlCarousel({
            //    navigation: true, // Show next and prev buttons
            //    slideSpeed: 300,
            //    paginationSpeed: 400,
            //    singleItem: true,
            //    navigationText: ["<img src='images/left-arrow.png'>", "<img src='images/right-arrow.png'>"]
            //    // "singleItem:true" is a shortcut for:
            //    // items : 1,
            //    // itemsDesktop : false,
            //    // itemsDesktopSmall : false,
            //    // itemsTablet: false,
            //    // itemsMobile : false
            //});
            $owl.owlCarousel({
                slideSpeed: 300,
                paginationSpeed: 400,
                items: 1,
                dots: true,
                loop: true,
                nav: true,
                navText: ["<img src='images/left-arrow.png'>", "<img src='images/right-arrow.png'>"]
            });



            
            // After destory, the markup is still not the same with the initial.
            // The differences are:
            //   1. The initial content was wrapped by a 'div.owl-stage-outer';
            //   2. The '.owl-carousel' itself has an '.owl-loaded' class attached;
            //   We have to remove that before the new initialization.
            //$owl.html($owl.find('.owl-stage-outer').html()).removeClass('owl-loaded');
            //$owl.owlCarousel({
            //    // your initial option here, again.
            //});


        });


        





        //$('.moe_instruction_min').hide();

    });
});
function setpolygonFitSize() {
    ////Fitting a set of layers inside the view.
    var layers = [
        '.Mall-Boundary-Layer',
        '.Units-Layer',
        '.Parking-Lots-Layer',
        '.Interior-ParkingLots-Layer'
    ];
    var speed = 1; //in seconds
    var padding = 50; //pixels
    building.fitPolygonInView(layers, speed, padding, function () {
        fitLabels();
    });
}
function searchCommonData() {
    //Reset(1);
    if ($("#Search-From").val().length > 0) {
        $("#ulSearchResult").html("");
        var inputText = $("#Search-From").val().toString().toLowerCase();
        var filteredDestination = $.grep(destinations, function (element, index) {
            return element.name.toLowerCase() !== inputText;
        });
        populateStore(filteredDestination);
    } else {
        $("#ulSearchResult").html("");
        var inputText = $("#Search-To").val().toString().toLowerCase();
        var filteredDestination = $.grep(destinations, function (element, index) {
            return element.name.toLowerCase() !== inputText;
        });
        populateStore(filteredDestination);
    }
}
//Bind the floors
function BindFloors(maps) {
    mapData = maps;
    var defaultMap;
    var renderString = "";

    for (var i = 0; i < maps.length; i++) {
        renderString += renderItem(maps[i]);
        if (maps[i].defaultMapForDevice === true) {
            defaultMap = maps[i].mapId;
            //$(".moe_display_floor").text(maps[i].locationname);
        }
    }
    $.when($("#floorIndicator").html(renderString)).then(function () {
        $(".floorItemInd[data=" + defaultMap + "]").addClass('selected');
        //$('.floorItemInd').on('click', function (evt) {
        $(document).on('click', '.floorItemInd', function (evt) {
            $('.floorItemInd').removeClass('selected');
            $(this).addClass('selected');
            //Reset(2);
            //switchFloor($(this).attr('data'));
            showFlooronMap($(this).find('.mml_text').html(), $(this).attr('data'));
            $('#amenityAlertBox').hide();
        });
    });
}
//Bind the Amentities
function Amentities() {
    JMap.getAmenities(function (res) {
        myAmenities = res;
    });
    var str = "", i;
    building.hideShowAmenities(false);
    var defaultAmenity = false;
    for (i = 0; i < myAmenities.length; i++) {
        if (myAmenities[i].localizedText.toLowerCase() == 'entrance') {
            building.showAmenityById(myAmenities[i].componentId, true);
            defaultAmenity = true;
        } else { defaultAmenity = false; }

        //str += "<li class='amenityItem displayAmenity " + (defaultAmenity ? 'BackgroundRed' : '') + "' status='" + (defaultAmenity ? true : false) + "' data-id='" + myAmenities[i].componentId + "'><a><span class='mml_img'>" + myAmenities[i].svg + "</span><div class='am-text'>" + myAmenities[i].localizedText + "</div></a></li>";
        //str += "<div class='amenityItem displayAmenity " + (defaultAmenity ? 'BackgroundRed' : '') + "' status='" + (defaultAmenity ? true : false) + "' data-id='" + myAmenities[i].componentId + "'><div>" + myAmenities[i].svg + "</div><span>" + myAmenities[i].localizedText + "</span></div>";
        str += "<div class='item amenityItem displayAmenity " + (defaultAmenity ? 'BackgroundRed' : '') + "' status='" + (defaultAmenity ? true : false) + "' data-id='" + myAmenities[i].componentId + "'><span class='mml_img'>" + myAmenities[i].svg + "</span></div>";
    }

    $.when($("#amenities").html(str)).then(function () {

        var owl = $("#amenities");

        //owl.owlCarousel({

        //    itemsCustom: [
        //      [0, 6],
        //      [430, 8],
        //      [600, 8],
        //      [700, 10],
        //      [850, 12]
        //    ],
        //    navigation: true,
        //    navigationText: ["<img src='images/left-arrow-mob.png'>", "<img src='images/right-arrow-mob.png'>"]

        //});


        owl.owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            //items: 1,
            dots: false,
            nav: true,
            //items : 15, //10 items above 1000px browser width
            //itemsDesktop: [1199, 15], //5 items between 1000px and 901px
            //itemsDesktopSmall: [979, 12], // betweem 900px and 601px
            //itemsTablet: [768, 10], //2 items between 600 and 0
            //itemsMobile: [479, 5], // itemsMobile disabled - inherit from itemsTablet option
            //itemsCustom: [
            //  [0, 6],
            //  [430, 8],
            //  [600, 8],
            //  [700, 10],
            //  [850, 12]
            //],
            navText: ["<img src='images/left-arrow.png'>", "<img src='images/right-arrow.png'>"],
            loop: false,
            //rewindNav:true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 2,
                    nav: true
                },
                300: {
                    items: 5,
                    nav: true
                },
                500:{
                    items: 7,
                    nav: true
                },
                700:{
                    items: 10,
                    nav: true
                },
                1000: {
                    items: 15,
                    nav: true,
                    //loop: false
                }
            }
        });

        owl.hide();

        $(".amenityItem").click(function () {
            // show amenity with id 1234

            building.hideShowAmenities(false);
            $(".amenityItem").removeClass("BackgroundRed");
            //debugger;

            var val = ($(this).attr('status') === "true");
            building.showAmenityById($(this).attr('data-id'), !val);
            $(".amenityItem").attr("status", false);
            $(this).attr("status", !val)

            clearAllMobileSubMenu();

            if ($(this).attr('status') === "true") {
                $(this).addClass("BackgroundRed");

                var amenity = JMap.getAmenityById(parseInt($(this).attr('data-id')));
                var landmarks = building.getLandmarksByMapId(building.currentFloor.id);
                const amenitiesOnMap = landmarks.filter(landmark => landmark.model.componentId === amenity.componentId)
                if (amenitiesOnMap.length === 0) {
                    var landmarksArray;
                    var amenitiesOnMapArray;
                    var amenityOnFloors = '';
                    for (var i = 0; i < building.mapsData.length; i++) {
                        landmarksArray = building.getLandmarksByMapId(building.mapsData[i].id);
                        amenitiesOnMapArray = landmarksArray.filter(landmark => landmark.model.componentId === amenity.componentId);
                        if (amenitiesOnMapArray.length > 0) {
                            amenityOnFloors += "<div class='floorItemInd' data='" + building.mapsData[i].id + "'><input type='button' value='" + building.mapsData[i].name + "' /></div>";
                        }
                    }
                    $('#amenityFloorList').html(amenityOnFloors);
                    $('#amenityAlertBox').show();
                }

            } else {
                $(this).removeClass("BackgroundRed");
            }



        });
    });

}
// render the html for floor 
function renderItem(item) {
    return "<li class='floorItemInd' data='" + item.mapId + "'>" + item.description + "</li>";
};
function BindCategories(data) {
    var renderString = '';
    dest = '', allItems = '';
    renderString += '<li><a class="categoryDropdown" data-id="0" href="#">Select Category</a></li>';
    for (var i = 0; i < data.length; i++) {
        if (data[i].id == 6244) {
            dest = JMap.getDestinationsByCategoryId(data[i].id);
            for (var n = 0; n < dest.length; n++) {
                building.highlightUnitsByDestinationId(dest[n].id, 0.3, '#9B9587');
            }
        }

        if (data[i].name.toLowerCase() == 'anchor stores') {
            anchorStoreId = data[i].id;
            setAnchorStores(anchorStoreId);
        }
        renderString += '<li><a class="categoryDropdown" data-id="' + data[i].id + '" href="#">' + data[i].name + '</a></li>';
    }
    //console.log(renderString);
    $('#CategoryListing').html(renderString);
    $.when($('#CatResContainer').html(allItems)).then(function () {
        //bindAccordian();
        $('.catDestinationResult').on('click', function (evt) {
            searchLevel = 1;
            destSelector(evt);
            Reset(2);
        });
    });
}
//populate the categories
function populateCategories(data) {
    var head = '', dest = '', body = '', categoryItem = '', allItems = '';
    for (var i = 0; i < data.length; i++) {
        // Get the subcategory by CategoryId
        dest = JMap.getDestinationsByCategoryId(data[i].id);

        if (data[i].id == 6244) {
            for (var n = 0; n < dest.length; n++) {
                building.highlightUnitsByDestinationId(dest[n].id, 0.3, '#9B9587');
            }
        }

        if (data[i].name.toLowerCase() == 'anchor stores') {
            anchorStoreId = data[i].id;
            setAnchorStores(anchorStoreId);
        }

        if (dest.length === 0) {
            continue;
        }
        //set the category title
        head = renderCategories(data[i]);
        //dest = JMap.getDestinationsByCategoryId(data[i].id);
        //render the destinations
        if (dest.length === 1) {
            body += renderDestination(dest[0]);
            if (renderDestination(dest[0]) === '') {
                head = '';
                body = '';
                categoryItem = '';
                continue;
            }
        } else {
            for (var j = 0; j < dest.length; j++) {
                body += renderDestination(dest[j]);
            };
        }
        //render both into a div
        categoryItem = renderAccordion(head, body);
        //add item into c=container variable
        allItems += categoryItem;
        //clear variables
        head = '';
        body = '';
        categoryItem = '';
    }
    $.when($('#CatResContainer').html(allItems)).then(function () {
        //bindAccordian();
        $('.catDestinationResult').on('click', function (evt) {
            searchLevel = 1;
            destSelector(evt);
            Reset(2);
        });
    });
}
function Reset(type) {
    if (type == 1) {


        //Remove the open class from Levels 
        $('.moe_menu .moe_menu_ul > li.moe_menu_level_1').removeClass("open");
        $('.moe_menu .moe_menu_ul > li.moe_menu_level_1 > ul').slideUp()

    } else if (type == 2) {
        $(".moe_alpha_result").slideUp();
        $(".moe_alfa_btn").removeClass("active");
        $(".moe_cat_result").slideUp();
        $(".moe_cat_btn").removeClass("active");
        $(".moe_keyword_result").slideUp();
    }

}
function renderCategories(cat) {
    var padding = "";
    return '<a class="moe_toggle moe_toggle_title" href="javascript:void(0);"  style="background-image:url(' + baseUrl + cat.iconPath + ');">' + cat.name + '</a>';
}
function renderDestination(d) {
    var getFloorObject = getFloorByMapId(d.waypoints[0].mapId);
    return getFloorObject != "" ? '<li data-id="' + d.id + '" class="catDestinationResult"><div class="moe_dest_name">' + d.name + '</div><div class="moe_dest_floor">' + getFloorObject + '</div></li>' : "";
    //return '<div data-id="' + d.id + '" class="catDestinationResult displayDestination" data-background-color-hover="webcolor2" data-background-color="webcolor1" data-color="secondary"><div class="catDestinationName">' + d.name + '</div><div class="catDestinationFloor">' + getFloor(d.id) + '</div></div>';
}
function getFloor(id) {
    var Floor = JMap.getFloorsByDestinationId(id);
    var floorName;
    if (Floor[0]) floorName = Floor[0].description;
    floorName = (floorName == undefined ? "" : floorName);
    return Floor ? floorName : "";
}
function getFloorByMapId(id) {
    var Floor = building.floors[id];
    var floorName;
    if (Floor) floorName = Floor.description;
    floorName = (floorName == undefined ? "" : floorName);
    return Floor ? floorName : "";
}
function getFloorTitleByMapId(id) {
    var Floor = building.floors[id];
    var floorName;
    if (Floor) floorName = Floor.floorTitle;
    floorName = (floorName == undefined ? "" : floorName);
    return Floor ? floorName : "";
}
function renderAccordion(head, body) {
    return '<li class="moe_accordion_item">' + head + '<ul class="moe_toggle_inner moe_search_sub_list">' + body + '</ul></li>';
}

//Bind the alphabetic categories
function populate(data) {

    var letterGroup = '';
    var currentLetter = "";
    var nextLetterGroup;
    var letterTitle;

    data.sort(function (a, b) {
        var textA = a.name.toUpperCase();
        var textB = b.name.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });


    for (var i = 0; i <= data.length; i++) {
        if (i == data.length) {

            htmlStr += renderGroup(letterTitle, letterGroup);
            continue;
        }
        //If location is not mapped
        //if(!data[i].wp) continue;

        var thisLetter = data[i].name.charAt(0);

        //if current letter does not match first letter of destination
        if (currentLetter != thisLetter) {
            //if first latter of destination is a number
            if (!isNaN(currentLetter) && isNaN(thisLetter)) {
                nextLetterGroup = true;
            }
            if (nextLetterGroup === true) {
                nextLetterGroup = false;
                if (letterTitle != undefined) {
                    htmlStr += renderGroup(letterTitle, letterGroup);
                }
                letterTitle = '';
                letterGroup = '';
            }

            if (isNaN(thisLetter) === false) {
                letterTitle = renderTitle('#');
                currentLetter = thisLetter.toUpperCase();

            } else if (thisLetter === '@') {
                currentLetter = data[i].name.charAt(1).toUpperCase();
            } else {
                letterTitle = renderTitle(thisLetter);
                currentLetter = thisLetter.toUpperCase();
                nextLetterGroup = true;
            }

            letterGroup += render(data[i]);
        } else {
            letterGroup += render(data[i]);
        }

    }


    $.when($("#AlphaResContainer").html(htmlStr)).then(function () {
        $('.destinationResult').on('click', function (evt) {
            searchLevel = 1;
            destSelector(evt);
            Reset(2);
        });
    })

}
function render(data) {
    var destinationFloor = getFloorByMapId(data.waypoints[0].mapId);
    return destinationFloor != "" ? '<li data-id="' + data.clientId + '" class="destinationResult displayDestination" ><div class="moe_dest_name">' + data.name + '</div><div class="moe_dest_floor destinationFloor">' + destinationFloor + '</div></li>' : "";
}
function renderTitle(alpha) {
    var id;
    if (alpha === '#') id = 'num';
    else id = alpha;
    //letterList.push(alpha);
    return '<h3 class="moe_search_title" id="result-' + id + '">' + alpha + '</h3>';
}
function renderGroup(title, results) {

    return '<li class="moe_search_item">' + title + '<ul class="moe_search_sub_list">' + results + '</ul></li>';
}
function populateStore(data) {
    var str = '';
    var res0 = '';
    var res1 = '';
    for (var i = 0; i < data.length; i++) {

        if (data[i]) {

            if (data[i].name != res0 && data[i].name != res1) {

                str += renderStore(data[i]);
            }
        }

    };

    if (str == "") {

        str = "<div class='noResultTxt' >No Records Found</div>";
    }

    $.when($('#ulSearchResult').html(str)).then(function () {

        $('.destinationKeyResult').on('click', function (evt) {

            //ga('send', 'event', 'button', 'click', 'storeselector');

            //Reset(2);
            destSelector(evt);
        });
    });
}
function renderStore(data) {
    var destinationFloor = getFloorByMapId(data.waypoints[0].mapId);
    //return destinationFloor != "" ? '<div data-id="' + data.id + '" class="destinationKeyResult displayDestination" data-background-color-hover="webcolor2" data-background-color="webcolor1" data-color="secondary"><div class="destinationKeyName">' + data.name + '</div><div class="destinationKeyFloor">' + destinationFloor + '</div></div>' : "";
    return destinationFloor != "" ? '<li data-id="' + data.id + '" class="destinationKeyResult displayDestination"><div class="categories">' + data.name + '</div><div class="categories-image">' + destinationFloor + '</div></li>' : "";
}
function DrawPath() {

    //$(".moed_store_details").css("display", "block");
    if ($("#Search-From").val().length > 0 && $("#Search-To").val().length > 0) {
        //$(".moe_get_direction").hide();
        //$(".moe_accessible_path").show();
        //$(".moe_replay").show();
        $(".moe_accessible_path").css('display', 'inline-block');
        $(".moe_replay").css('display', 'inline-block');
        $('.markerDialogue').remove();

        var _from = JMap.getDestinationById($("#Search-From").attr("data"))
        var _to = JMap.getDestinationById($("#Search-To").attr("data"));

        //var Floor = JMap.getFloorsByDestinationId(_from.id);
        //var Floor = building.floors[_from.waypoints[0].mapId];
        //$('.moe_details_name').html(_from.name + '-' + Floor.floorTitle);
        //$(".moe_details_info").html(_from.category[0]);



        // display way path animation with levels
        displayWayAnimation(_from, _to);

        building.wayfind(_from, _to, {
            useElevator: isElevator, // Forces path with elevators only
            animate: true, // Animates the drawn path
            approxEntrance: false // Draws path to second last waypoint on path.
        });
        //debugger;
        //setpolygonFitSize();
        //selectionZoomLevel = 0;
        //setZoomInOutImage();


        var zoom = 0;
        var speed = 1;
        //Focus to Waypoint
        //var waypoint = getSomeInterestingWaypoint();
        building.focusToWp(_from.waypoints[0], zoom, speed);
        //setpolygonFitSize();
        //selectionZoomLevel = zoom;
        //setZoomInOutImage();


        //displayMarchingAnt();
        isPathDisplay = true;
        building.unbindMapInteraction();
        $('#CloseButton').show();
    } else {
        if ($("#Search-From").val().length == 0 && $("#Search-To").val().length > 0) {
            //$("#mapDestination").show();
            //$(".accessClass").hide();
            //$("#floorBreadCrumbs").hide();
        } else {
            //if ($("#DetailsContainer").hasClass("opened"))
            //    $("#DetailsContainer").removeClass("opened");
            //$("#mapDestination").hide();
            building.resetUnitHighlight();
            building.resetAllMaps();
            setAnchorStores(anchorStoreId);
        }

    }
}
function destSelector(objResult) {
    //debugger;
    var res = objResult.currentTarget
    $(".searchClear").show();


    var selectedDestination = $(res);
    var Dest = JMap.getDestinationById(selectedDestination.attr('data-id'));
    var arr = [];
    arr.push(Dest);

    if (searchLevel == 1) {
        setFirstSelection(Dest);
        //open the store map
    } else if (searchLevel == 2) {
        $("#Search-From").attr("data", Dest.id);
        $("#Search-From").val(Dest.name);
        //DrawPath();
    } else if (searchLevel == 3) {
        //debugger;
        $("#Search-To").val(Dest.name);
        $("#Search-To").attr("data", Dest.id);
        //var Floor = JMap.getFloorsByDestinationId(Dest.id);
        var Floor = building.floors[Dest.waypoints[0].mapId];
        var dataId = Floor.id;
        //$(".moe_display_floor").text(Floor.floorTitle);
        building.resetUnitHighlight();
        setAnchorStores(anchorStoreId);
        $.when(building.showFloor(Floor.id)).then(function () {
            //$(".floorItemInd").find('img').attr('src', 'Images/8053_1_5989_moe_btn_menu_inactive.png');
            //$(".floorItemInd[data=" + dataId + "]").find('img').attr('src', 'Images/8051_1_5989_moe_btn_menu_active.png');
            $(".floorItemInd").removeClass('selected');
            $(".floorItemInd[data=" + Floor.id + "]").addClass('selected');
            var type = "destination";
            var state = true;
            var id = Dest.id;
            var fill = highlightColor;
            var speed = 1;
            building.highlightUnitsBy(type, state, id, fill, speed);

        });

        //$('.moe_details_name').html(Dest.name + '-' + Floor.floorTitle);
        //$(".moe_details_info").html(Dest.category[0]);

        //open the store map

        //DrawPath();
    }
}
$(document).on('click', '.SearchSwap', function (ev) {

    var inputText = $("#Search-To").val();
    var dataId = $("#Search-To").attr("data");
    $("#Search-To").val($("#Search-From").val());
    $("#Search-To").attr("data", $("#Search-From").attr("data"));

    $("#Search-From").val(inputText);
    $("#Search-From").attr("data", dataId);

    searchLevel = 2;
    //DrawPath();
});
$(document).on('click', '.moe_get_direction', function () {
    //$("#Search-To").next().show();
    searchLevel = 2;
    //$(".moe_search_box").hide();
    //$(".moe_search_way").show()
    if ($("#moe_search_input_main").val().length > 0) {
        $("#Search-To").val($("#moe_search_input_main").val());
        $("#Search-To").attr("data", $("#moe_search_input_main").attr("data"));
    } else {
        if ($("#Search-To").val().length == 0) {
            $("#Search-To").val(selectedDestName);
            $("#Search-To").attr("data", selectedDestId);
        }

    }

    $("#Search-From").focus();
    $("#moe_search_input_main").val("")
    var dummyDestination = [];
    dummyDestination = $.grep(destinations, function (n, i) {
        return n.name !== $("#Search-To").val();
    });

    if (dummyDestination.length > 0) {

        $(".moe_keyword_result").slideToggle('slow').toggleClass("open");
        $.when($("#ulSearchResult").html("")).then(function () {
            populateStore(dummyDestination);
        });

    }
    // setTimeout(function () { $(this).removeClass("BTNon"); }, removeButtonAnimation);

    clearAllMobileSubMenu();
    clearSearchContainer();
    $('#fromToContainer').show();
    $('#dvSearchSubContainer').show();
    //$('#searchContainer').show();
    //$('#SearchSourceDestination').hide();
    //$('#SearchToDestination').show();

    //$('.search-result').css('height', $(window).height() - $('#SearchToDestination').outerHeight());
    $('#markerPopup').remove();

});
$(document).on('keyup', '#Search-From', function (ev) {
    $("#ulSearchResult").html("");
    var inputText = $(this).val().toString().toLowerCase();
    var filteredDestination = $.grep(destinations, function (element, index) {
        if ($("#Search-To").val().length > 0) {
            $(this).next().show();
            return element.name.toLowerCase().indexOf(inputText) !== -1 && element.name.toLowerCase() !== $("#Search-To").val().toLowerCase();
        }
        else {
            $(this).next().hide();
            return element.name.toLowerCase().indexOf(inputText) !== -1;
        }
    });
    populateStore(filteredDestination);
});
$("#Search-From").click(function () {
    searchCommonData()
    searchLevel = 2;
});
$("#moe_search_input_main").click(function () {
    searchLevel = 1;
});
$("#Search-To").click(function () {
    searchCommonData();
    searchLevel = 3;
});
$(document).on('keyup', '#Search-To', function (ev) {

    $("#ulSearchResult").html("");
    var inputText = $(this).val().toString().toLowerCase();
    var filteredDestination = $.grep(destinations, function (element, index) {
        if ($("#Search-From").val().length > 0) {
            //$(this).next().show();
            return element.name.toLowerCase().indexOf(inputText) !== -1 && element.name.toLowerCase() !== $("#Search-From").val().toLowerCase();
        }
        else {
            //$(this).next().hide();
            return element.name.toLowerCase().indexOf(inputText) !== -1;
        }
    });
    populateStore(filteredDestination);
});
function displayWayAnimation(_from, _to) {

    //console.log(_from);
    //console.log(_to);

    $.when(building.floors[_from.waypoints[0].mapId], building.floors[_to.waypoints[0].mapId]).then(function (fromFloor, toFloor) {
        var levelClick = "showFlooronMap('" + toFloor.floorTitle + "', " + toFloor.id + ")";
        var routeAnimationHTML = '<div id="moe_route_holder" class="moe_route_holder"><div class="moe_route_line"></div><div class="moe_route_arrows trigUpActive"></div>';
        routeAnimationHTML += '<div class="moe_route_icon RII BTN moe_start_breadcrumb moe_pulse_effect" id="endPoint" onclick="' + levelClick + '"><span>' + toFloor.description + '</span></div>';

        var midpoints = ((toFloor.sequence > fromFloor.sequence) ? (toFloor.sequence - fromFloor.sequence) : (fromFloor.sequence - toFloor.sequence));
        var isIncrease = ((toFloor.sequence > fromFloor.sequence) ? true : false);

        var showLeft = 0;
        if (midpoints > 0) {
            showLeft = parseInt((100 / (midpoints)));
        }
        commonTimeOut = (midpoints + 1) * commonTimeOut;

        if (midpoints > 1) {
            for (var i = 1; i < midpoints; i++) {
                var currentLevelDesc = ('L' + (isIncrease ? ((fromFloor.sequence + i) - 1).toString() : ((fromFloor.sequence - 1) - i).toString())).toUpperCase();
                var currentLevelObject = $.grep(mapData, function (n, i) {
                    return n.description.toUpperCase() == currentLevelDesc;
                });
                levelClick = "showFlooronMap('" + currentLevelObject[0].name + "', " + currentLevelObject[0].id + ")";
                routeAnimationHTML += '<div class="moe_route_icon RII BTN moe_end_breadcrumb" onclick="' + levelClick + '" style="left:' + (((showLeft * i) - (5 - (i == 1 ? 1 : 0) + (i > 1 ? 1 : 0)))).toString() + '%" id="midPoint' + (i.toString()) + '">';
                routeAnimationHTML += '<span>L' + (isIncrease ? ((fromFloor.sequence + i) - 1).toString() : ((fromFloor.sequence - 1) - i).toString()) + '</span></div>';
            }
        }
        levelClick = "showFlooronMap('" + fromFloor.floorTitle + "', " + fromFloor.id + ")";
        routeAnimationHTML += '<div class="moe_route_icon RII BTN moe_pulse_effect moe_end_breadcrumb" id="startPoint" onclick="' + levelClick + '">';
        routeAnimationHTML += '<span class="selectedWidgetIcon">' + fromFloor.description + '</span></div></div>';

        if (midpoints > 0) {
            showLeft = parseInt((80 / (midpoints)));
        }

        if (fromFloor.sequence != toFloor.sequence) {
            routeAnimationHTML += '<div id="moe_mover_icon1" style="left: ' + (midpoints > 1 ? (((0 + showLeft) / 2)).toString() + "%" : "36%") + '; display: block;">';
            routeAnimationHTML += '<div class="moe_mover_icon"><img src="' + (isElevator ? "images/elevator.png" : "images/steps-mobile.png") + '"><span></span></div></div>';
        }

        if (midpoints > 1) {
            for (var i = 1; i < midpoints; i++) {
                routeAnimationHTML += '<div id="moe_mover_icon' + (i + 1).toString() + '" style="left: ' + ((((showLeft * i) + (showLeft * (i + 1))) / 2) - (2 * (i))) + "%" + '; display: block;">';
                routeAnimationHTML += '<div class="moe_mover_icon"><img src="' + (isElevator ? "images/elevator.png" : "images/steps-mobile.png") + '"><span></span></div></div>';
            }
        }

        routeAnimationHTML += '<div id="floorHolder"></div>';
        $('#floorBreadCrumbs').html(routeAnimationHTML);

        //$('.moe_store_info').removeClass('w80').addClass('w40');

        //$('.moe_store_nav').show();

        //$('#jmapFooter').addClass('moe_m_state2');
        clearAllMobileSubMenu();
        clearSearchContainer();
        $('#dvSearchSubContainer').show();
        $('#directionAnimator').show();

        levelClick = '';
    });
}
function showFlooronMap(floorName, floorId) {
    //$(".moe_display_floor").text(floorName);
    //Switch floor by floor ID

    JMap.configuration.mapStyles.mapConfig.floorSwitch.type = 'fade';
    $.when(building.showFloor(floorId)).then(function (e) {
        //$(".floorItemInd").removeClass("indActive");
        //$(".floorItemInd").find('img').attr('src', 'Images/8053_1_5989_moe_btn_menu_inactive.png');
        //$(".floorItemInd[data=" + floorId + "]").find('img').attr('src', 'Images/8051_1_5989_moe_btn_menu_active.png');
        $(".floorItemInd").removeClass('selected');
        $(".floorItemInd[data=" + floorId + "]").addClass('selected');


    });
}
function addMapInteractionClick(destination, waypoint, event) {
    setFirstSelection(destination);
}
function setRedPathLineUI() {
    var paths = building.container.querySelectorAll('.floor-path > svg')
    for (var i = 0; i < paths.length; i++) {
        var svgPath = paths[i].firstChild
        if (svgPath) {

            paths[i].firstChild.style.strokeWidth = 7;
            paths[i].firstChild.style.strokeOpacity = 1;
            paths[i].firstChild.style.stroke = '#FFD311';

            if (paths[i].getElementsByClassName('marching-ants').length <= 0) {
                var clonePath = svgPath.cloneNode();
                clonePath.style.stroke = '#9D1619'
                clonePath.style.strokeWidth = 5
                clonePath.style.strokeOpacity = 1
                clonePath.style.strokeDasharray = 7
                clonePath.classList.add('marching-ants')
                paths[i].appendChild(clonePath)
            }
        }
    }
    setCountReadPathUI += 1;
    if (setCountReadPathUI > 8) {
        clearInterval(marchAntInterval);
    }

}
function showCustomPopup(Destination) {
    var wps = Destination.waypoints[0];
    $('.markerDialogue').remove();
    var markup = '';

    markup = '<div class="direction-box markerDialogue" id="markerPopup"><img alt="Source Store" src="images/dest.png"></div>';

    //markup = '<div class="direction-box markerDialogue" id="markerPopup"><div class="direction-details"><a href="#" class="searchClear"><img src="images/close-2.png" alt="close"></a><div class="address">';
    //markup += '<p>' + Destination.name + '</p><p>' + Destination.category[0] + '</p><div class="level">' + getFloorTitleByMapId(wps.mapId) + '</div></div></div>';
    ////markup += '<div class="contact"><div class="contact-number">+97142943537</div><div class="contact-img"><a href="#"><img src="images/contact.png" alt="contact"></a></div><div class="clearfix"></div></div>';
    //markup += '<div class="map-direction"><div class="map-text"><a href="#" class="moe_get_direction">GET DIRECTION</a></div><div class="map-img"><a href="#"><img src="images/location.png" alt="get"></a></div><div class="clearfix"></div></div></div>';

    building.showCustomPopup(wps.mapId, 'popup', markup, {
        x: wps.x,
        y: wps.y
    });
}

function switchFloor(mapId, callback) {

    JMap.configuration.mapStyles.mapConfig.floorSwitch.type = 'fade';
    var currentFloor = building.currentFloor
    var newFloor = building.floors[mapId]
    var directionValue = currentFloor.sequence > newFloor.sequence ? -1 : 1;
    if (typeof callback !== 'function') callback = function () { }

    building.createFloorSwitchAnimation(
      { alpha: 1, y: 0 },
      { alpha: 0, y: 3500 * directionValue },
     { alpha: 0, y: -3500 * directionValue },
      { alpha: 1, y: 0 }
    );

    if (currentFloor.id === mapId) callback();
    else building.switchFloor(mapId, true, callback);
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function focusWaypoint() {
    // Get the waypoint by id
    const wpId = Number(this.dataset.waypointId)
    const waypoint = JMap.getWpById(wpId);
    // Same Building object passed on init complete
    const building = JMap.storage.maps.building;

    if (waypoint) {
        const zoom = 2;
        const speed = 0.3;
        //JMap.showPoint([waypoint.x, waypoint.y])
        building.focusToWp(waypoint, zoom, speed);
    }
}
function displaySourceFromParam() {
    var destIdParam = getParameterByName('destId');
    if (parseInt(destIdParam)) {
        var getDestination = JMap.getDestinationById(destIdParam);
        if (getDestination != null) {
            var destinationFloor = JMap.getFloorsByDestinationId(destIdParam);
            if (destinationFloor[0] != undefined) {
                building.showFloor(destinationFloor[0].id);
            }
            if (getDestination != undefined && getDestination != null) {
                addMapInteractionClick(getDestination);
            }
        }
    }
}
function setAnchorStores(categoryId) {
    dest = JMap.getDestinationsByCategoryId(categoryId);
    if (dest.length > 0) {
        for (var n = 0; n < dest.length; n++) {
            building.highlightUnitsByDestinationId(dest[n].id, 0.3, '#e4cba4');
        }
    }
}
function fitLabels() {
    var floor = building.currentFloor
    var inlays = building.getInlayLabelsByMapId(floor.id)
    inlays.forEach(function (label) {
        if (label.lboxData.height > label.lboxData.width) {
            label.container.rotation = label.lboxData.rotation + 90
            var w = label.lboxData.width
            var h = label.lboxData.height
            label.lboxData.width = h
            label.lboxData.height = w
        }
        label.rewrapText()
        building.touchMap()
    })
}
function removeDestinationHighlight() {
    $('.markerDialogue').remove();
    building.resetUnitHighlight();

}
function setFirstSelection(Dest) {

    // set selected store unit with search text box
    $("#moe_search_input_main").val(Dest.name);
    $("#moe_search_input_main").attr("data", Dest.id);

    clearAllMobileSubMenu();
    clearSearchContainer();
    $('#dvSearchSubContainer').show();
    $('#selectedSourceLocation').show();


    var Floor = building.floors[Dest.waypoints[0].mapId];
    var dataId = Floor.id;
    
    $("#selectedStoreLevel").text(Floor.floorTitle);
    $('#selectedStoreName').html(Dest.name);
    $("#selectedStoreCategory").html(Dest.category[0]);
    showCustomPopup(Dest);

    building.resetUnitHighlight();
    setAnchorStores(anchorStoreId);
    
    $.when(building.showFloor(dataId)).then(function () {
        $(".floorItemInd").removeClass('selected');
        $(".floorItemInd[data=" + dataId + "]").addClass('selected');
        var type = "destination";
        var state = true;
        var id = Dest.id;
        var fill = highlightColor;
        var speed = 1;
        building.highlightUnitsBy(type, state, id, fill, speed);
    });
    

    const zoom = 2;
    const speed = 1;
    //Focus to Waypoint
    building.focusToWp(Dest.waypoints[0], zoom, speed);
    

    // Set all labels for map
    building.eachFloor(function () {
        var labels = building.getInlayLabelsByMapId(this.id);
        if (labels != null && labels != undefined && labels != '') {
            labels.forEach(label => label.changeFontColor('#666666'));
        }
        
    });
    // set selected store unit label
    var label = building.getInlayLabelByWaypointId(Dest.wps[0].id)
    if (label != null && label != undefined && label != '') {
        label.changeFontColor('#FFFFFF');
    } 
}

/* Start of New HTML jQuery */


function filterStoreResult(searchText, CategoryId) {
    $("#ulSearchResult").html("");
    var filteredDestination = $.grep(destinations, function (element, index) {
        return element.name.toLowerCase().indexOf(searchText) !== -1 && (CategoryId == 0 || element.categoryId[0] == CategoryId);
    });
    populateStore(filteredDestination);
}
function clearAllMobileSubMenu() {
    //$('#searchContainer').hide();
    //$('#selectedSourceLocation').hide();
    //$('#fromToContainer').hide();
    //$('#directionAnimator').hide();
    $('#attractionContainer').hide();
    $('#amenities').hide();
    $('#dvSearchResult').hide();
    $('#dvSliderInstruction').hide();
    $('#amenityAlertBox').hide();
    $('#dvSearchSubContainer').hide();
}
function clearSearchContainer() {
    $('#searchContainer').hide();
    $('#selectedSourceLocation').hide();
    $('#fromToContainer').hide();
    $('#directionAnimator').hide();
}
$("#CloseButton").click(function () {
    //$(".moed_store_details").css("display", "none");
    //$(".moe_accessible_path").hide();
    //$(".moe_replay").hide();
    //$(".moe_get_direction").show();
    //$('.moe_details_name').html("");
    //$(".moe_details_info").html("");
    //$(".moe_search_box").show();
    //$(".moe_search_way").hide();
    $("#moe_search_input_main").val("");
    $("#moe_search_input_main").attr("data", "");
    $("#Search-To").val("");
    $("#Search-To").attr("data", "");
    $("#Search-From").val("");
    $("#Search-From").attr("data", "");
    $(".searchClear").click();
    //$('#SearchSourceDestination').show();
    //$('#SearchToDestination').hide();
    //$('.search-result').removeAttr('style');
    //$('#SearchToDestination').removeAttr('style');
    //$('.moe_instruction').hide();
    //$('#jmapFooter').hide();
    $("#CloseButton").hide();
    searchLevel = 1;
    clearAllMobileSubMenu();
    clearSearchContainer();
    $('#searchContainer').show();
    building.resetAllMaps();
    building.resetUnitHighlight();
    building.clearYah();
    setAnchorStores(anchorStoreId);
    building.addMapInteraction("touchend click", function (destination, waypoint, event) {
        addMapInteractionClick(destination[0], waypoint, event);
    });
    building.eachFloor(function () {
        const labels = building.getInlayLabelsByMapId(this.id);
        labels.forEach(label => label.changeFontColor('#666666'));
    });
});
$(document).on('click', '.searchClear', function () {
    //$(".searchClear").click(function () {
    $(this).hide();
    $(this).prev().val("");
    //$(this).prev().attr("data", "");
    //$(".moed_store_details").hide();
    isPathDisplay = false;
    if (searchLevel == 2 || searchLevel == 3) {
        //if ($("#Search-From").val().length == 0 && $("#Search-To").val().length == 0) {
        //    $(".moe_search_box").show();
        //    $(".moe_search_way").hide();
        //} else {

        //    $(".moe_search_box").hide();
        //    $(".moe_search_way").show();
        //}
        building.resetUnitHighlight();
        building.clearYah();
        building.resetAllMaps();
        //$('.waypathInstruction').hide();
    }
    removeDestinationHighlight();
    setAnchorStores(anchorStoreId);
    //$('#instructions').hide();
    //$('.moe_instruction_min').hide();
    ////$('#floorBreadCrumbs').hide();
    //$('.moe_store_nav').hide();
    //$('.moe_store_info').removeClass('w40').addClass('w80');
    building.eachFloor(function () {
        const labels = building.getInlayLabelsByMapId(this.id);
        labels.forEach(label => label.changeFontColor('#666666'));
    });
});
$('#infoMap').click(function () {
    $('#infoBoxdetail').show();
});
$('#infoClose').click(function () {
    $('#infoBoxdetail').hide();
});
$(".search-text").click(function () {
    $(".dropdown-text").show();
});
$(".search-list").click(function () {
    $(".category-drop").toggle();
    $(".dropdown-text").show();
});

function ConsumeMOEService() {
    //var url = "http://www.malloftheemirates.com/ws/ws_mafMobileApplication.asmx";
    //$.ajax({
    //    type: "GET",
    //    url: url + "/GetSiteID",
    //    //data: "{variant_id:'1'}",
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    async: true,
    //    success: OnSuccessCall,
    //    error: OnErrorCall
    //});

    //function OnSuccessCall(response) {
    //    alert(response.d);
    //}


    //function OnErrorCall(response) {
    //    alert(response.status + " " + response.statusText);
    //}

    //$.ajax({
    //    crossDomain: true,
    //    type: "GET",
    //    contentType: "application/json; charset=utf-8",
    //    async: false,
    //    url: "http://www.malloftheemirates.com/ws/ws_mafMobileApplication.asmx/GetSiteID?callback=?",
    //    //data: { projectID: 1 },
    //    dataType: "jsonp",
    //    jsonpCallback: 'fnsuccesscallback'
    //});

    //function fnsuccesscallback() {
    //    alert("yesh");
    //}

}
function soap() {
//    var xmlhttp = new XMLHttpRequest();
//    xmlhttp.open('POST', 'http://www.malloftheemirates.com/ws/ws_mafMobileApplication.asmx', true);

//    // build SOAP request
//    var strRequest =
//        //'<?xml version="1.0" encoding="utf-8"?>' +
//        //'<soapenv:Envelope ' +
//        //    'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
//        //    'xmlns:api="http://www.malloftheemirates.com/ws/ws_mafMobileApplication.asmx/GetSiteID" ' +
//        //    'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ' +
//        //    'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
//        //    //'<soapenv:Body>' +
//        //    //    '<api:some_api_call soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
//        //    //        '<username xsi:type="xsd:string">login_username</username>' +
//        //    //        '<password xsi:type="xsd:string">password</password>' +
//        //    //    '</api:some_api_call>' +
//        //    //'</soapenv:Body>' +
//        //'</soapenv:Envelope>';
//   //var sr =
////    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' +
////'<s:Body><GetSiteID xmlns="http://www.maf.ae/" xmlns:a="http://schemas.datacontract.org/2004/07/ConsumeService.MOEService"' +
////    'xmlns:i="http://www.w3.org/2001/XMLSchema-instance"/></s:Body></s:Envelope> ';
//"<?xml version='1.0' encoding='utf-8'?>";
//    strRequest = strRequest + "<soap:Envelope " +
//"xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
//"xmlns:xsd='http://www.w3.org/2001/XMLSchema' " +
//"xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>";
//    strRequest = strRequest + " <soap:Body>";
//    strRequest = strRequest + "<GetSiteID xmlns='http://www.maf.ae/' />";
//    strRequest = strRequest + "</soap:Body>";
//    strRequest = strRequest + "</soap:Envelope>";

//    xmlhttp.onreadystatechange = function () {
//        if (xmlhttp.readyState == 4) {
//            if (xmlhttp.status == 200) {
//                debugger;
//                alert('done. use firebug/console to see network response');
//            }
//        }
//    }
//    // Send the POST request
//    xmlhttp.setRequestHeader('Content-Type', 'text/xml');
//    xmlhttp.setRequestHeader("SOAPAction", "http://www.maf.ae/GetSiteID");
//    xmlhttp.send(strRequest);
//    // send request
//    // ...


//    //$.ajax({
//    //    type: 'POST',
//    //    url: 'http://www.malloftheemirates.com/ws/ws_mafMobileApplication.asmx',
//    //    contentType: 'text/xml; charset=utf-8',
//    //    Host: 'www.malloftheemirates.com',
//    //    headers: {
//    //        SOAPAction: 'http://www.maf.ae/GetSiteID',
//    //            Host: 'www.malloftheemirates.com'
//    //    },
//    //    data: '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><GetSiteID xmlns="http://www.maf.ae/" xmlns:a="http://schemas.datacontract.org/2004/07/ConsumeService.MOEService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"/></s:Body></s:Envelope>',
//    //    success: successFn,
//    //    error: errorFn
//    //});
    
//    //function successFn(data) {
//    //    alert("you did it");
//    //}
//    //function errorFn(e) {
//    //    alert("ohh sorry");
//    //}

//    $.ajax({
//        type: "POST",
//        async: true,
//        url: "http://www.malloftheemirates.com/ws/ws_mafMobileApplication.asmx",
//        //data: '{ "strName":"' + strProjectName + '","strLanguageId":"' + strLanguageId + '","Key": "CreateProject"}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data) {
//            alert(data);
//            if (data != null) {
//                //alert(data.d);
//                //args.IsValid = data.d;
//            }
//        }
//    });

}
//$('#canvasContainer').click(function () {
//    clearAllMobileSubMenu();
//});
$('#hlFoodCourt').click(function () {
    setAttraction(7442, 'food court');
});
$('#hlCinema').click(function () {
    setAttraction(7441, 'cinemas');
});
$('#hlKidsEntertaintmenet').click(function () {
    setAttraction(7440, 'entertainment');
});
function setAttraction(attrCategoryId, CategoryText) {
    clearAllMobileSubMenu();
    removeDestinationHighlight();
    setAnchorStores(anchorStoreId);

    var filteredCategory = $.grep(categories, function (element, index) {
        return element.name.toLowerCase().indexOf(CategoryText) !== -1;
    });

    if (filteredCategory.length == 0) {
        filteredCategory = $.grep(categories, function (element, index) {
            return element.id == attrCategoryId;
        });
    }
    if (filteredCategory.length > 0) {
        dest = JMap.getDestinationsByCategoryId(filteredCategory[0].id);
        var isFocusToWayPoint = false;  
        if (dest.length > 0) {
            for (var n = 0; n < dest.length; n++) {
                if (dest[n].waypoints[0].mapId == building.currentFloor.id) {
                    if (!isFocusToWayPoint) {
                        setZoomToDestination(dest[n].waypoints[0], 2, 0.8);
                        isFocusToWayPoint = true;
                    }
                }
                building.highlightUnitsByDestinationId(dest[n].id, 0.3, '#CE3750');

                var label = building.getInlayLabelByWaypointId(dest[n].wps[0].id);
                if (label != null && label != undefined && label != '') {
                    label.changeFontColor('#FFFFFF');
                }

            }
            if (!isFocusToWayPoint) {
                for (var i = 0; i < building.mapsData.length; i++) {
                    var GetDestinationAsFloor = $.grep(dest, function (element, index) {
                        return element.waypoints[0].mapId == building.mapsData[i].mapId;
                    });
                    if (GetDestinationAsFloor.length > 0) {
                        setZoomToDestination(GetDestinationAsFloor[0].waypoints[0], 2, 0.8);
                        $('.floorItemInd').removeClass('selected');
                        $(".floorItemInd[data=" + GetDestinationAsFloor[0].waypoints[0].mapId + "]").addClass('selected');
                        break;
                    }
                }
            }
        }
    }
}
function setZoomToDestination(wayPoints, zoom, speed) {
    building.focusToWp(wayPoints, zoom, speed);
    //selectionZoomLevel = zoom;
    //setZoomInOutImage();
}
$('#dvDrawPath').click(function () { DrawPath(); });
$('#dvToggle').click(function () { clearAllMobileSubMenu(); });